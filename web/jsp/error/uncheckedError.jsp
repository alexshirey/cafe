<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Unchecked error page</title>
</head>
<body>

<br/>
Sorry, we couldn't perform your request...
<br/>
<hr>

<br/>
Response from ${pageContext.errorData.requestURI} failed
<br/>
Status code: ${pageContext.errorData.statusCode}
<br/>
exception: ${pageContext.exception}
<br/>
caused by: ${pageContext.exception.cause}
<br/>
<br/>

<a href="${root}/index.jsp">go to main page</a>

</body>
</html>
